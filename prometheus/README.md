# cedp-prometheus

## Prometheus
The prometheus config file at /etc/prometheus/prometheus.yml includes the metrics from the cedp tools

## Usage
```bash
oc new-app https://gitlab.com/mike-lam/cedp-prometheus --strategy=docker
oc expose service/cedp-prometheus
```

