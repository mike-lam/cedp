oc new-project cedp
export PROJECT=$(oc project --short)
oc adm policy add-scc-to-user anyuid  system:serviceaccount:${PROJECT}:default
./gitea.sh
./jenkins.sh
./sonarqube.sh
./artifactory.sh
./grafana.sh
oc get all

