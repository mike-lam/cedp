dbname=$1
user=$2
password=$3
GRP=$4

PROJECT=$(oc project --short)

oc get services postgresql-$1 2> /dev/null
if [ $? -eq 0 ]
then
  echo "postgresql-$1 already setup"
else
  echo "create  postgresql-$1"
  oc adm policy add-scc-to-user anyuid  system:serviceaccount:${PROJECT}:default
  oc get templates -n openshift -o custom-columns=NAME:.metadata.name|grep -i ^postgres
  oc new-app --name=postgresql-$1 --template=postgresql-ephemeral -p DATABASE_SERVICE_NAME=postgresql-$1 -p POSTGRESQL_USER=$user -p POSTGRESQL_PASSWORD=$password -p POSTGRESQL_DATABASE=$dbname -l ${GRP}
#  oc expose service/postgresql-$1
fi

oc get services pgadmin4 2> /dev/null
if [ $? -eq 1 ]
then
  echo "create pgadmin4"
  oc new-app dpage/pgadmin4  --name "pgadmin4" -e "PGADMIN_DEFAULT_EMAIL=user@domain.com" -e "PGADMIN_DEFAULT_PASSWORD=admin"
  oc expose service/pgadmin4
else
  echo "pgadmin already setup"
fi
