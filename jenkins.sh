PROJECT=$(oc project --short)
GRP=app.kubernetes.io/part-of=jenkins-grp
oc delete all -l ${GRP}
oc adm policy add-scc-to-user anyuid  system:serviceaccount:${PROJECT}:default
#define plugin and their dependencies
GITEA=display-url-api:2.3.5,handy-uri-templates-2-api:2.1.8-1.0,gitea:1.2.1
oc new-app jenkins-persistent \
        --name jenkins-cedp \
        -p ENABLE_OAUTH=false \
        -e INSTALL_PLUGINS=${GITEA},\
antisamy-markup-formatter:2.1,\
apache-httpcomponents-client-4-api:4.5.13-1.0,\
authentication-tokens:1.4,\
blueocean:1.24.7,\
blueocean-autofavorite:1.2.4,\
blueocean-bitbucket-pipeline:1.24.7,\
blueocean-commons:1.24.7,\
blueocean-config:1.24.7,\
blueocean-core-js:1.24.7,\
blueocean-dashboard:1.24.7,\
blueocean-display-url:2.4.1,\
blueocean-events:1.24.7,\
blueocean-git-pipeline:1.24.7,\
blueocean-github-pipeline:1.24.7,\
blueocean-i18n:1.24.7,\
blueocean-jira:1.24.7,\
blueocean-jwt:1.24.7,\
blueocean-personalization:1.24.7,\
blueocean-pipeline-api-impl:1.24.7,\
blueocean-pipeline-editor:1.24.7,\
blueocean-pipeline-scm-api:1.24.7,\
blueocean-rest:1.24.7,\
blueocean-rest-impl:1.24.7,\
blueocean-web:1.24.7,\
bootstrap4-api:4.6.0-3,\
bootstrap5-api:5.0.1-2,\
bouncycastle-api:2.20,\
branch-api:2.6.2,\
caffeine-api:2.9.1-23.v51c4e2c879c8,\
checks-api:1.7.0,\
cloudbees-bitbucket-branch-source:2.9.9,\
cloudbees-folder:6.15,\
command-launcher:1.6,\
conditional-buildstep:1.4.1,\
config-file-provider:3.8.0,\
configuration-as-code:1.51,\
credentials:2.5,\
credentials-binding:1.26,\
docker-commons:1.17,\
docker-workflow:1.26,\
durable-task:1.37,\
echarts-api:5.1.2-2,\
external-monitor-job:1.7,\
favorite:2.3.2,\
font-awesome-api:5.15.3-3,\
git:4.7.2,\
git-client:3.7.2,\
git-server:1.9,\
github:1.33.1,\
github-api:1.123,\
github-branch-source:2.9.9,\
github-organization-folder:1.6,\
google-oauth-plugin:1.0.6,\
groovy:2.4,\
handlebars:3.0.8,\
htmlpublisher:1.25,\
jackson2-api:2.12.3,\
javadoc:1.6,\
jaxb:2.3.0.1,\
jdk-tool:1.5,\
jenkins-design-language:1.24.7,\
jira:3.3,\
jjwt-api:0.11.2-9.c8b45b8bb173,\
jquery3-api:3.6.0-1,\
jsch:0.1.55.2,\
junit:1.51,\
kubernetes:1.30.0,\
kubernetes-client-api:5.4.1,\
kubernetes-credentials:0.9.0,\
ldap:1.26,\
lockable-resources:2.11,\
mailer:1.34,\
matrix-auth:2.6.7,\
matrix-project:1.18,\
maven-plugin:3.8,\
mercurial:2.15,\
metrics:4.0.2.8,\
momentjs:1.1.1,\
oauth-credentials:0.4,\
openshift-client:1.0.34,\
openshift-sync:1.0.46,\
parameterized-trigger:2.39,\
pipeline-build-step:2.13,\
pipeline-github-lib:1.0,\
pipeline-graph-analysis:1.11,\
pipeline-input-step:2.12,\
pipeline-milestone-step:1.3.2,\
pipeline-model-api:1.8.5,\
pipeline-model-declarative-agent:1.1.1,\
pipeline-model-definition:1.8.5,\
pipeline-model-extensions:1.8.5,\
pipeline-rest-api:2.19,\
pipeline-stage-step:2.5,\
pipeline-stage-tags-metadata:1.8.5,\
pipeline-stage-view:2.19,\
pipeline-utility-steps:2.8.0,\
plugin-util-api:2.3.0,\
popper-api:1.16.1-2,\
popper2-api:2.5.4-2,\
prometheus:2.0.10,\
run-condition:1.5,\
scm-api:2.6.4,\
script-security:1.77,\
snakeyaml-api:1.29.1,\
sse-gateway:1.24,\
ssh-credentials:1.18.1,\
structs:1.23,\
subversion:2.14.4,\
token-macro:2.13,\
trilead-api:1.0.13,\
variant:1.4,\
windows-slaves:1.8,\
workflow-aggregator:2.6,\
workflow-api:2.46,\
workflow-basic-steps:2.23,\
workflow-cps:2.92,\
workflow-cps-global-lib:2.19,\
workflow-durable-task-step:2.39,\
workflow-job:2.41,\
workflow-multibranch:2.24,\
workflow-remote-loader:1.5,\
workflow-scm-step:2.13,\
workflow-step-api:2.23,\
workflow-support:3.8\
        -l ${GRP} 



while [ -n "$(oc get route jenkins 2>&1 > /dev/null)" ]; do
  sleep 0.1
done

$(oc get pods  | grep "jenkins-.*-.*Running" | grep -v "deploy" 2>&1 > /dev/null)
while [  $? -eq 1 ]; do
        oc get pods  | grep "jenkins-.*Running"
  sleep 0.1
  $(oc get pods  | grep "jenkins-.*-.*Running" | grep -v "deploy")
done


PODNAME=$(oc get pods  | grep "jenkins-.*-.*Running" | grep -v "deploy" | awk  '{ print $1 }')
echo $PODMAN
oc rsync ./jenkins/ ${PODNAME}:/var/lib/jenkins/ -c jenkins --progress #set config as code
oc delete pod ${PODNAME} #restart jenkins
