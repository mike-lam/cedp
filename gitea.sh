PROJECT=$(oc project --short)
GRP=app.kubernetes.io/part-of=gitea-grp

oc get services gitea  2> /dev/null
if [ $? -eq 0 ]
then
  echo "gitea already setup"
else 
  oc adm policy add-scc-to-user anyuid  system:serviceaccount:${PROJECT}:default
  dbname=$(cat gitea/app.ini | grep  'Postgres Configuration' -A 10 | grep 'NAME ' | awk  '{print $3}')
  user=$(cat gitea/app.ini | grep  'Postgres Configuration' -A 10 | grep 'USER ' | awk  '{print $3}')
  password=$(cat gitea/app.ini | grep  'Postgres Configuration' -A 10 | grep 'PASSWD ' | awk  '{print $3}')
  ./postgre.sh $dbname $user $password $GRP

  oc new-app https://gitlab.com/mike-lam/cedp \
             --name=gitea \
	     --context-dir=gitea \
	     --strategy=docker \
	     -l ${GRP}
  oc expose service/gitea
  ROUTE=$(oc get route gitea -o jsonpath='{.spec.host}{"\n"}')
  echo "check https://docs.gitea.io/en-us/database-prep/#postgresql-1"
  echo "and https://docs.gitea.io/en-us/config-cheat-sheet/#database-database"

  echo "Database Type: PostgreSQL"
  echo "Host: postgresql-$dbname:5432  because is name of postgresal sevice for gitea"
  echo "Username: $user    because of role name"
  echo "Password: $password    because of password in role" 
  echo "Database Name: $dbname   becuse of create database"
  echo "Gitea Base URL: http://"$ROUTE"    get from route name" 
  echo "Administrator Username:admin1"
  echo "Password:••••"
  echo "Email Address: a@b.c"
  echo "All of this will create the file /var/lib/gitea/custom/conf in the gitea container"
fi

