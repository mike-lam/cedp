GRP=app.kubernetes.io/part-of=monitoring-grp
PROJECT=$(oc project --short)

oc adm policy add-scc-to-user anyuid  system:serviceaccount:${PROJECT}:default
dbname=$(cat grafana/grafana.ini | grep  'database' -A 10 | grep 'name =' | awk  '{print $3}')
user=$(cat grafana/grafana.ini | grep  'database' -A 10 | grep 'user =' | awk  '{print $3}')
password=$(cat grafana/grafana.ini | grep  'database' -A 10 | grep 'password =' | awk  '{print $3}')
./postgre.sh $dbname $user $password $GRP
oc new-app https://gitlab.com/mike-lam/cedp \
           --name=grafana-cedp \
           --context-dir=grafana \
	   --strategy=docker \
	   -l ${GRP} 
oc expose service/grafana-cedp

oc new-app https://gitlab.com/mike-lam/cedp \
           --name=prometheus-cedp \
	   --context-dir=prometheus \
	   --strategy=docker \
	   -l ${GRP} 
#using --name=prometheus causes a build storm
oc expose service/prometheus-cedp

oc new-app quay.io/prometheus/node-exporter:latest \
	    --name=node-exporter \
	    -l ${GRP}
oc expose service/node-exporter

# to login in grafana use admin and admin
# in grafana add datasource: use url=http://prometheus:9090 
# prometheus config file os at /etc/prometheus/prometheus.yml

