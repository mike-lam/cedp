# CEDP

## gitc.sh
- is a convenience script to clone and deploy on an openshift learn instance

## cedp.sh 
- calls the tools scripts to deploy in oc

## gitea.sh
### 1st deployment
- call postgre.sh gitea
- creates the giteadb in postgreSQL
- deploy gitea in oc
### 1st startup
-- copy paste the url in the FIXME
-- add admin user
### sanity check 
- this sanity check will be used later on in jenjins
- migrate https://github.com/TTFHW/jenkins_pipeline_hello
- migrate https://github.com/mike-lam/java-hello-world-with-maven
- then make Jenkinsfile with this text
```
pipeline {  
  agent  any
    
  stages {
    
    stage("Build") {
      steps {
        echo "Building the application using Maven (pom.xml)"
        //withMaven() {
          sh "mvn package -Dskip.unit.tests=true"
        //}
      }
    }
    
    stage("Unit & Integration Tests") {
      steps {
        echo "Unit & Integration Tests"
        //withMaven() {
          sh "mvn test -Dskip.unit.tests=false"
        //}
      } 
    }      
  }

  post { 
    always { 
      deleteDir() 
    }
  }
}
```


## grafana.sh
- deploy prometheus in oc
- deploy grafana in oc

To login in grafana use admin and admin

In grafana add datasource: use url=http://prometheus-cedp:9090
using name prometheus causes a build storm

prometheus config file os at /etc/prometheus/prometheus.yml

node_exporter takes metrics on all the cluster

To use node_exporter in grafana import dashboard 1860. 

###References
- https://github.com/prometheus/node_exporter

## jenkins
### 1st deployment
- only the openshift supplied image works
  - to customize use info at https://docs.openshift.com/container-platform/4.1/openshift_images/using_images/images-other-jenkins.html
- the script will use code as infrastructure principle and copy ./jenkins in the container
- the container will restart
- because of code as infrastructure credentials gets corupted so enter them MANUALLY
- on startup when getting Your connection is not private, use advance and then Proceed to jenkins-*.environments.katacoda.com (unsafe)
- Authentication
  - using OATH=true (default), 
    - authentication is done by openshift
    - u=admin p=admin
    - allow selected permissions
  - using OATH=false
    - authentication is done by jenkins 
    - u=admin p=password  
-oc expose jenkins-persistent not required because the template does it! 

### sanity check
- add item admin1 which will scan admin1 and build jenkins_pipeline_hello and java-hello-world-with-maven

## postgre.sh
- deploy a postgreSQL database with a suffix passed as agument
- deploy pgAdmin4 if not yet deployed



