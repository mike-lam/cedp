
# cedp-gitea

## Gitea
The gitea config file at /etc/gitea/custom/conf/app.ini includes some default starting values

Creating and populating the database is out of scope

When 1st starting confirm these settings
- Database Type: PostgreSQL"
- Host: postgresql-gitea:5432  //assumes is name of postgresSQL sevice for gitea
- Username: gitea    //assumes is role name in db
- Password: gitea    //assumes is password in role"
- Database Name: giteadb   //assumes is create database sql
- Gitea Base URL: http://gitea-....environments.katacoda.com/    //get from route name
- Administrator Username:admin1 
- Password:••••"
- Email Address: a@b.c"

All of this will create the file /var/lib/gitea/custom/conf in the gitea container"

## Usage
```bash
  oc new-app https://gitlab.com/mike-lam/cedp-gitea --strategy=docker
  oc expose service gitea
```
## References
-  https://docs.gitea.io/en-us/database-prep/#postgresql-1
-  https://docs.gitea.io/en-us/config-cheat-sheet/#database-database
